class GameSimulation
  @@n = 10000
  
  def initialize(attackers, defenders, args={})
    @attackers = attackers
    @defenders = defenders
    @bonus_attack = args[:bonus_attack] || 0
    @bonus_defense = args[:bonus_defense] || 0
    @first_strike = args[:first_strike] || false
    @berserk = args[:berserk] || false
    
    @win_percentage = 0
    @loss_percentage = 0
    @draw_percentage = 0
    @massacre_percentage = 0
    
    @killed_one_warior = 0
    @attackers_lost_killing_one = 0
  
    @win_survivors = 0
    @loss_survivors = 0
    @draw_attack_survivors = 0
    @draw_defense_survivors = 0
    
    @title = assemble_title
  end
  
  def run_simulation(number_of_waves = 3, retreat_after_first_kill = false)
    wins = 0
    losses = 0
    draws = 0
    massacres = 0
    
    killed_one_warrior = 0
    attackers_lost_killing_one = 0
    
    win_survivors = 0
    loss_survivors = 0
    draw_attack_survivors = 0
    draw_defense_survivors = 0
    
    @@n.times do 
      results = simulate_battle(false)
      retreat_results = simulate_battle(true)
      
      if results[:defenders] < @defenders
        killed_one_warrior += 1
        attackers_lost_killing_one += @attackers - retreat_results[:attackers]
      end
        
      if results[:attackers] == 0 and results[:defenders] == 0
        massacres += 1
      elsif results[:attackers] == 0
        losses += 1
        loss_survivors += results[:defenders]
      elsif results[:defenders] == 0
        wins += 1
        win_survivors += results[:attackers]
      else
        draws += 1
        draw_attack_survivors += results[:attackers]
        draw_defense_survivors += results[:defenders]
      end
               
    end
    
    @win_percentage = (wins / Float(@@n)) * 100
    @loss_percentage = (losses / Float(@@n)) * 100
    @draw_percentage = (draws / Float(@@n)) * 100
    @massacre_percentage = (massacres / Float(@@n)) * 100
    
    @killed_one_warrior = (killed_one_warrior / Float(@@n)) * 100
    @attackers_lost_killing_one = (attackers_lost_killing_one / Float(killed_one_warrior))
    
    @win_survivors = win_survivors / Float(wins) if wins > 0
    @loss_survivors = loss_survivors / Float(losses) if losses > 0
    @draw_attack_survivors = draw_attack_survivors / Float(draws) if draws > 0
    @draw_defense_survivors = draw_defense_survivors / Float(draws) if draws > 0
  end
  
  def display_data
    puts "===#{@title}==="
    puts "Win percent: #{@win_percentage.round(2)}"
    puts "-- Survivors: #{@win_survivors.round(2)}"
    puts "Loss percent: #{@loss_percentage.round(2)}"
    puts "-- Survivors: #{@loss_survivors.round(2)}"
    puts "Draw percent: #{@draw_percentage.round(2)}"
    puts "-- Attacker Survivors: #{@draw_attack_survivors.round(2)}"
    puts "-- Defender Survivors: #{@draw_defense_survivors.round(2)}"
    puts "Massacre percent: #{@massacre_percentage.round(2)}"
    puts ""
    puts "Chance to kill a group of island defenders: #{(@win_percentage + @massacre_percentage).round(2)}"
    puts "Chance to kill one warrior: #{@killed_one_warrior.round(2)}"
    puts "Warriors lost killing one warrior: #{@attackers_lost_killing_one.round(2)}"
    puts ""
  end
  
  private
  
  def assemble_title
    title = ""
    if(@berserk)
      title += "Berserk "
    elsif(!@first_strike)
      title += "Vanilla "
    end
    
    if(@first_strike)
      title += "First Strike "
    end
    
    title += @attackers.to_s
    if(@bonus_attack > 0)
      title += "+#{@bonus_attack}"
    end
    
    title += "x#{@defenders.to_s}"
    if(@bonus_defense > 0)
      title += "+#{@bonus_defense}"
    end
    
    return title
  end
  
  def simulate_battle(retreat_after_first_kill = false)
    attackers = @attackers
    defenders = @defenders
    wave = 1
    
    #Simulate the three waves. Variable i stores the current wave (either 0, 1, or 2)
    begin
      attack_kills = 0
      defense_kills = 0
      
      #Create a new array with a number of elements equal to the number of dice rolled by the attacker
      #Pass a block to the array that populates it with random values from 1 to 6
      attack_rolls = Array.new(attackers + @bonus_attack) {rand(1..6)}
      
      if @berserk
        #Count how many elements of the array match the condition in block passed to the count method
        #The condition tests of the number is inside the range between two numbers
        attack_kills += attack_rolls.count {|x| (3..6) === x}
        defense_kills += attack_rolls.count {|x| (1..2) === x}
      else
        attack_kills += attack_rolls.count {|x| ((7 - wave)..6) === x}
      end
      
      if @first_strike
        defenders -= attack_kills
        defenders = 0 if defenders < 0 
      end
      
      defense_rolls = Array.new(defenders + @bonus_defense) {rand(1..6)}
      defense_kills += defense_rolls.count {|x| ((7 - wave)..6) === x}
      
      unless @first_strike
        defenders -= attack_kills
        defenders = 0 if defenders < 0 
      end
      
      attackers -= defense_kills
      attackers = 0 if attackers < 0
      
      if attackers == 0 or defenders == 0 or wave == 3
        #Cease combat if one side or the other is dead or it's the end of the third wave
        continue_combat = false
      elsif retreat_after_first_kill and !@berserk and defenders < @defenders
        #Cease combat early if the attacker has gotten a kill and isn't a berserker
        continue_combat = false
      else
        continue_combat = true
        wave += 1
      end
    end while(continue_combat)
    
    return {attackers: attackers, defenders: defenders}
  end
end

sim = GameSimulation.new(3,4, berserk: true)
sim.run_simulation
sim.display_data

sim2 = GameSimulation.new(3,4, berserk: true, bonus_attack: 1)
sim2.run_simulation
sim2.display_data

